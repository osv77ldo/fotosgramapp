import { Component, OnInit } from "@angular/core";
import { User } from 'src/app/interfaces/interfaces';
import { UserService } from 'src/app/services/user.service';
import { NgForm } from '@angular/forms';
import { UiServiceService } from 'src/app/services/ui-service.service';

@Component({
  selector: "app-tab3",
  templateUrl: "tab3.page.html",
  styleUrls: ["tab3.page.scss"],
})
export class Tab3Page implements OnInit {

  user: User = {};

  constructor( private userService: UserService, private uiService: UiServiceService) {}

  ngOnInit() {
    this.user = this.userService.getUser();
    console.log(this.user);
  }

  async updateUser( formUpdateUser: NgForm) {
    const validUpdate = await this.userService.updateUser(this.user);

    if(validUpdate) {
      this.uiService.presentToast("User Updated", "success");
    }else{
      this.uiService.presentToast("Invalid operation", "danger");
    }
  }

  logout() {
    this.userService.logout();
  }
}
