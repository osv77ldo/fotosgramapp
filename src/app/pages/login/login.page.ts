import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { IonSlides, NavController } from "@ionic/angular";
import { UserService } from "src/app/services/user.service";
import { UiServiceService } from 'src/app/services/ui-service.service';
import { User } from 'src/app/interfaces/interfaces';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  @ViewChild("mainSlide", { static: false }) slides: IonSlides;

  loginUser = {
    email: "test",
    password: "1234",
  };

  registerUser: User = {
    email: 'test',
    password: '123456',
    name: 'test',
    avatar: 'av-1.png'
  };

  constructor(
    private userService: UserService,
    private navCtrl: NavController,
    private uiService: UiServiceService
  ) {}

  ngOnInit() {
    // this.slides.lockSwipes(true);
  }

  async login(formLogin: NgForm) {

    if (formLogin.invalid) {
      return;
    }

    const validUser = await this.userService.login(
      this.loginUser.email,
      this.loginUser.password
    );

    if (validUser) {
      this.navCtrl.navigateRoot('main/tabs/tab1', {animated: true});
    } else {
      this.uiService.presentInfoAlert('Incorrect User and Password');
    }
  }

  async register(formRegister: NgForm) {

    const validRegister = await this.userService.register(this.registerUser);
    if (validRegister) {
      this.navCtrl.navigateRoot('main/tabs/tab1', {animated: true});
    } else {
      this.uiService.presentInfoAlert('Incorrect Register');
    }
  }

  showRegisterForm() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
  }

  showLoginForm() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
  }
}
