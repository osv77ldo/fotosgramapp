import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Post } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  posts: Post[] = [];
  enableInfiniteScroll = true;

  constructor( private postService: PostsService) {}

  ngOnInit() {
    this.nexts();

    this.postService.newPost
    .subscribe( post => {
      this.posts.unshift( post );
    });
  }

  nexts( event?, pull: boolean = false ) {

    this.postService.getPosts( pull )
    .subscribe( resp => {
      console.log(resp.posts);
      this.posts.push(...resp.posts);

      if ( event ) {
        event.target.complete();
        if (resp.posts.length === 0){
          this.enableInfiniteScroll = false;
        }
      }
    });
  }

  refresh( event?) {
    this.posts = [];
    this.enableInfiniteScroll = true;
    this.nexts( event, true);
  }

}
