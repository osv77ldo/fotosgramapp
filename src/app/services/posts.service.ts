import { Injectable, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { ResponsePosts, Post } from "../interfaces/interfaces";
import { UserService } from './user.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';


const URL = environment.url;

@Injectable({
  providedIn: "root",
})
export class PostsService {
  pagePosts = 0;

  newPost = new EventEmitter<Post>();

  constructor(private http: HttpClient,
              private userService: UserService,
              private fileTransfer: FileTransfer ) {}

  getPosts(pull: boolean = false) {
    if (pull) {
      this.pagePosts = 0;
    }
    this.pagePosts++;

    return this.http.get<ResponsePosts>(`${URL}/post?page=${this.pagePosts}`);
  }

  createPost( post ) {
    const headers = new HttpHeaders({
      'x-token': this.userService.token
    });

    return new Promise(resolve => {

      this.http.post(`${URL}/post`, post, {headers})
      .subscribe(resp => {
        this.newPost.emit(resp['post']);
        resolve(true)
      })
    });
  }

  uploadImage(img: string) {
    const options: FileUploadOptions = {
      fileKey: 'image',
      headers: {
        'x-token': this.userService.token
      }
    };

    const fileTransfer: FileTransferObject = this.fileTransfer.create();
    fileTransfer.upload(img, `${URL}/post/upload`, options)
    .then(data => {
      console.log(data);
    }).catch(err => {
      console.log('error uploading file', err);
    });
  }
}
