import { Component, OnInit, Input, ViewChild } from "@angular/core";

declare var mapboxgl: any;

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"],
})
export class MapComponent implements OnInit {
  @Input() coords: string;
  @ViewChild('mapId', { static: true}) mapId;

  constructor() {}

  ngOnInit() {
    const latLng = this.coords.split(',');
    const lat = Number(latLng[0]);
    const long = Number(latLng[1]);

    mapboxgl.accessToken =
      "pk.eyJ1Ijoib3N2NzdsZG8iLCJhIjoiY2thYWlzdHl4MTRueDJ5cXZreWthbm82bSJ9.-XuwjgZ6ujz1wBLepbUXXw";
    const map = new mapboxgl.Map({
      container: this.mapId.nativeElement,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [long, lat],
      zoom: 15
    });

    const marker = new mapboxgl.Marker()
    .setLngLat( [long,lat])
    .addTo( map );
  }
}
